Mistral Dashboard Release Notes
===============================

Contents
========

.. toctree::
   :maxdepth: 2

   unreleased
   2024.1
   zed
   yoga
   xena
   wallaby
   victoria
   ussuri
   train
   stein
   rocky
   queens
   pike
   ocata
   newton
   mitaka
   liberty
